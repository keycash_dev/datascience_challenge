# Desafio Data Science - KeyCash

## Introdução

---

A KeyCash é uma proptech que compra imóveis, para depois reformá-los e recolocá-los no mercado. Para fazer isso de forma ágil e justa para todas as partes, utilizamos muita tecnologia e, principalmente, muitos dados. 

Dentre os diversos desafios que temos em Data Science na KeyCash, precificar os imóveis da melhor maneira possível é um deles. Essa etapa é importante, pois sem ela, não conseguimos otimizar e agilizar o processo de compra e venda. Nosso trabalho é ajudar o time comercial a entender o preço do apartamento e chegar em propostas mais justas e rápidas. 

Dado esse cenário, apresentaremos um problema real que enfrentamos. **Esperamos uma solução que possa ser apresentada para nosso time comercial, tomadores de decisão e, inclusive, nossos fundadores.**

## Desafio  

---

O time comercial precisa da nossa ajuda para decidir, assertivamente, se compram ou não imóveis que entraram no sistema. Para isso eles querem saber qual o preço ideal desses imóveis.

### Objetivo

O objetivo é desenvolver um modelo de regressão que seja capaz de precificar os imóveis ofertados pelos clientes. Porém, apenas o modelo não é o suficiente. Queremos entender o porquê dele ser bom e como mostrar para o comercial que eles podem confiar nele para tomar decisões. 

Alguns pontos importantes para o desafio:

- Precificação dos imóveis
- Composição do preço
- Por que devemos confiar nele?

### Dados

Os dados para resolução do problema estão no arquivo "base". No arquivo "ofertas" estão alguns imóveis para compra que devem ser precificados para o time comercial.

## Entrega

---

Acreditamos que uma semana é o suficiente para desenvolver o desafio, porém, caso necessite de mais tempo, entre em contato conosco. Para nos enviar o desafio ou caso tenha alguma dúvida, mande um e-mail para: vagas@keycash.io

Esperamos receber:

- Um arquivo (jupyter, excel ou algum outro de sua escolha) contendo a análise exploratória dos dados, o modelo desenvolvido* e a avaliação e interpretação dele.

- Outro arquivo (powerpoint, pdf etc.) com a apresentação final, feita numa linguagem que pessoas não técnicas possa entender.**

\* A escolha do algoritmo (e biblioteca) é livre, contanto que consiga explicar o funcionamento dele e o porquê da escolha;  

\** A apresentação não deve levar mais que 30 minutos.

## Dicas

---

- Não seja herói... Existe muita informação na internet, utilize sua criatividade para alavancar no que for necessário

- Fique a vontade para entrar em contato e tirar dúvidas sobre o problema, compartilhar as dúvidas costuma ser sempre bom.